package be.helha.tcpip.gui;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ResourceBundle;

/**
 * Panneau de base visible bien dans lorsqu'on clique sur le bouton 'À propos'
 * ainsi que lorsqu'on clique sur 'Aide' puis 'À propos'
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 */
public class PanelAboutBase extends JPanel implements GUIDefaultValues
{
    /**
     * Constructeur pour la base des différents panneaux 'À propos'.
     *
     * @param rb traducteur des chaînes de caractères
     */
    public PanelAboutBase( ResourceBundle rb )
    {
        setLayout( new BorderLayout() );
        setBackground( backgroundColor );

        StringBuilder sb = new StringBuilder();
        sb.append( rb.getString( "title" ) ).append( "\n" );
        sb.append( rb.getString( "string_written_and_developed_by" ) ).append( ":\n" );
        sb.append( "Giorgio Caculli LA196672" ).append( "\n" );
        sb.append( "Guillaume Lambert LA198116" ).append( "\n" );
        sb.append( "Tanguy Taminiau LA199566" ).append( "\n" );
        sb.append( rb.getString( "string_course_name" )  ).append( ":\n" );
        sb.append( "Isabelle Boulogne" ).append( "\n" );
        sb.append( rb.getString( "string_year" ) ).append( " 2021-2022\n" );

        JLabel aboutLabel = new JLabel( rb.getString( "menu_item_about" ), SwingConstants.CENTER );
        aboutLabel.setFont( font );

        JTextArea aboutText = new JTextArea();
        aboutText.setText( sb.toString() );
        aboutText.setWrapStyleWord( true );
        aboutText.setLineWrap( true );
        aboutText.setEditable( false );
        aboutText.setFocusable( false );
        aboutText.setOpaque( false );
        aboutText.setAlignmentX( Component.CENTER_ALIGNMENT );
        aboutText.setFont( font );
        aboutText.setBackground( backgroundColor );

        add( aboutLabel, BorderLayout.PAGE_START );
        add( aboutText, BorderLayout.CENTER );
    }
}
