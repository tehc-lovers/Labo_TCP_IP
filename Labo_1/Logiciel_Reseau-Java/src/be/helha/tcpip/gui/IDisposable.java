package be.helha.tcpip.gui;

import javax.swing.JOptionPane;
import java.awt.Component;
import java.awt.Window;
import java.util.ResourceBundle;

/**
 * Interface qui définit qu'une classe est capable d'arrêter le programme.
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 */
public interface IDisposable
{
    /**
     * Cette fonction ouvrira un pop-up dès que la fermeture de la fenêtre
     * est en cours. L'utilisateur pourra choisir si OUI ou NON il/elle désire
     * fermer le logiciel complètement.
     *
     * @param component l'entité qui fait appel à la fonction
     * @param rb le traducteur du message de fermeture
     */
    default void disposeWindowConfirm( Component component, ResourceBundle rb )
    {
        int choice = JOptionPane.showConfirmDialog( component.getParent(), rb.getString( "option_pane_window_closing_message" ) );
        if ( choice == JOptionPane.YES_OPTION )
        {
            ( ( Window ) component.getParent() ).dispose();
        }
    }
}
