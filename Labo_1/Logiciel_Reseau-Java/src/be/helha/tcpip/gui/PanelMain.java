package be.helha.tcpip.gui;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;
import java.awt.CardLayout;
import java.util.ResourceBundle;

/**
 * Panneau principale contenant les différentes cartes avec lesquelles
 * l'utilisateur pourra interagir.
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 * @see JPanel
 */
public class PanelMain extends JPanel implements GUIDefaultValues, ICardHandler
{
    /**
     * Constructeur du panneau qui contiendra tous les autres panneaux.
     *
     * @param rb traducteur de chaînes de caractères
     */
    public PanelMain( ResourceBundle rb )
    {
        setLayout( new CardLayout() );
        Border border = BorderFactory.createEmptyBorder( 20, 20, 20, 20 );
        setBorder( border );
        setBackground( backgroundColor );

        add( new PanelMenu( rb ), "panel.menu" );
        add( new PanelFunction1( rb ), "panel.function.1" );
        add( new PanelFunction2( rb ), "panel.function.2" );
        add( new PanelFunction3( rb ), "panel.function.3" );
        add( new PanelFunction4( rb ), "panel.function.4" );
        add( new PanelFunction5( rb ), "panel.function.5" );
        add( new PanelAbout( rb ), "panel.about" );

        showCard( this, "panel.menu" );
    }
}
