package be.helha.tcpip.gui;

import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import java.awt.Color;
import java.awt.Font;
import java.awt.KeyboardFocusManager;

/**
 * Interface contenant les informations qui caractérisent chaque element
 * graphique.
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 * @see DocumentListener;
 * @see Document;
 * @see DocumentFilter;
 */
public interface GUIDefaultValues
{
    /** Le nombre maximal de bits dans une adresse */
    short MAX_CHARACTERS_IPV4_BITS = 3;
    /** Le nombre maximal de bits dans une subnet */
    short MAX_CHARACTERS_NETMASK = 2;
    /** La longueur d'un IPv4, points compris */
    short MAX_LENGTH_IPV4_STRING = 7;
    /** La longueur d'un IPv4, points compris avec subnet */
    short MAX_LENGTH_IPV4_STRING_WITH_NETMASK = MAX_LENGTH_IPV4_STRING + 2;
    /** Taille de la police par défaut partout dans le programme */
    short FONT_SIZE = 24;
    /** Séparation entre les éléments à partir du haut */
    short INSET_TOP = 5;
    /** Séparation entre les éléments à partir de la gauche */
    short INSET_LEFT = 0;
    /** Séparation entre les éléments à partir du bas */
    short INSET_BOTTOM = 5;
    /** Séparation entre les éléments à partir du de la droite */
    short INSET_RIGHT = 0;
    /** Couleur affichée partout en arrière-plan */
    Color backgroundColor = new Color( 238, 232, 213 );
    /** Nature de la police visible partout dans le logiciel */
    Font font = new Font( Font.SANS_SERIF, Font.PLAIN, FONT_SIZE );

    /**
     * Autofocus listener pour les input des JTextField IPV4
     *
     * @see javax.swing.JTextField
     */
    PanelFunctionBase.AutoFocusTransferListener listenerIPV4 = new PanelFunctionBase.AutoFocusTransferListener()
    {
        @Override
        public void focusTransferShouldOccur( Document d )
        {
            KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
        }
    };

    /**
     * Autofocus listener pour les input des JTextField Netmask
     *
     * @see javax.swing.JTextField
     */
    PanelFunctionBase.AutoFocusTransferListener listenerNetMask = new PanelFunctionBase.AutoFocusTransferListener()
    {
        @Override
        public void focusTransferShouldOccur( Document d )
        {
            KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
        }
    };

    /**
     * Delegate qui impose un nombre maximal de caractères.
     */
    PanelFunctionBase.AutoFocusTransferDelegate delegateIPV4 = new PanelFunctionBase.MaxLengthAutoFocusTransferDelegate( MAX_CHARACTERS_IPV4_BITS );

    /**
     * Delegate qui va automatiquement transférer le focus au prochain JTextField.
     */
    DocumentListener documentListenerIPV4 = new PanelFunctionBase.AutoFocusTransferHandler( listenerIPV4, delegateIPV4 );

    /**
     * Delegate qui impose un nombre maximal de caractères.
     */
    PanelFunctionBase.AutoFocusTransferDelegate delegateNetMask = new PanelFunctionBase.MaxLengthAutoFocusTransferDelegate( MAX_CHARACTERS_NETMASK );

    /**
     * Delegate qui va automatiquement transférer le focus au prochain JTextField.
     */
    DocumentListener documentListenerNetMask = new PanelFunctionBase.AutoFocusTransferHandler( listenerNetMask, delegateNetMask );

    /**
     * Listener qui va imposer un nombre maximal de caractères dans son JTextField.
     */
    DocumentFilter filterIPV4 = new PanelFunctionBase.JTextFieldLimit( MAX_CHARACTERS_IPV4_BITS );

    /**
     * Listener qui va imposer un nombre maximal de caractères dans son JTextField.
     */
    DocumentFilter filterNetMask = new PanelFunctionBase.JTextFieldLimit( MAX_CHARACTERS_NETMASK );
}
