package be.helha.tcpip.gui;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

/**
 * Classe contenant la MenuBar visible en haut de la fenêtre.
 *
 * @author Caculli Giorgio
 * @author Lambert Guillaume
 * @author Taminiau Tanguy
 * @version 1.0
 * @see JMenuBar
 * @see JMenuItem
 * @see JMenu
 */
public class TopMenuBar extends JMenuBar implements IDisposable
{
    /** Traducteur de chaînes de caractères */
    private final ResourceBundle rb;
    /** Menu fichier à partir duquel l'utilisateur peut fermer le programme */
    private JMenu fileMenu = null;
    /** Menu aide à partir duquel l'utilisateur peut afficher le 'À propos' */
    private JMenu helpMenu = null;
    /** Panneau à propos à charger */
    private PanelAboutBase pa = null;

    /**
     * Constructeur pour la barre en haut du programme.
     *
     * @param rb traducteur de chaînes de caractères
     */
    public TopMenuBar( ResourceBundle rb )
    {
        this.rb = rb;
        pa = new PanelAboutBase( rb );
        initMenus();
    }

    /**
     * Fonction qui sert à initialiser les différents menus présents dans la
     * barre.
     */
    private void initMenus()
    {
        fileMenu = new JMenu( rb.getString( "menu_file" ) );
        helpMenu = new JMenu( rb.getString( "menu_help" ) );

        initMenuItems();

        add( fileMenu );
        add( helpMenu );
    }

    /**
     * Fonction qui sert à initialiser les sous-menus présents dans chaque
     * menu.
     */
    private void initMenuItems()
    {
        JMenuItem quitMenuItem = new JMenuItem( rb.getString( "menu_item_quit" ) );
        JMenuItem aboutMenuItem = new JMenuItem( rb.getString( "menu_item_about" ) );

        quitMenuItem.addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                disposeWindowConfirm( getRootPane(), rb );
            }
        } );

        aboutMenuItem.addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                JOptionPane.showMessageDialog( null, pa, rb.getString( "menu_item_about" ), JOptionPane.PLAIN_MESSAGE );
            }
        } );

        fileMenu.add( quitMenuItem );
        helpMenu.add( aboutMenuItem );
    }
}
