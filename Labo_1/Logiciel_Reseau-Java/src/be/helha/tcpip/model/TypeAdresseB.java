package be.helha.tcpip.model;

/**
 * Instanciation d'une adresse de classe B
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 * @see TypeAdresse
 */
public class TypeAdresseB extends TypeAdresse
{
    /**
     * Constructeur pour le type d'adresse B avec le masque par défaut.
     */
    public TypeAdresseB()
    {
        super( new AdresseMasque( "255", "255", "0", "0" ) );
    }

    /**
     * Fonction qui renvoie sa classe sous forme textuelle.
     *
     * @return "B"
     */
    @Override
    public String getTypeStr()
    {
        return "B";
    }
}
