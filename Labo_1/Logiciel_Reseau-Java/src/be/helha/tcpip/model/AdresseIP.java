package be.helha.tcpip.model;

/**
 * Classe d'une adresse IP.
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @see Adresse
 */
public class AdresseIP extends Adresse
{
    /**
     * Constructeur d'une Adresse IP.
     * Dans ce cas si, un octet ne peut pas avoir une valeur plus petite que 0
     * ou plus grande que 255.
     *
     * @param octet1 le premier octet de l'adresse IP
     * @param octet2 le deuxième octet de l'adresse IP
     * @param octet3 le troisième octet de l'adresse IP
     * @param octet4 le quatrième octet de l'adresse IP
     * @Pre 255 >= octet1>= 0 && 255 >= octet2 >= 0 && 255 >= octet3 >= 0 && 255 >= octet4 >= 0
     * @Post octetsDecimal.length == 4
     */
    public AdresseIP( String octet1, String octet2, String octet3, String octet4 )
    {
        super( octet1, octet2, octet3, octet4 );
    }

    /**
     * Constructeur d'une adresse IP via un tableau d'octets.
     *
     * @Pre ipBits.length == 4
     * @param ipBits tableau d'octets en format String
     */
    public AdresseIP( String[] ipBits )
    {
        super( ipBits );
    }

    /**
     * Fonction qui sert à convertir une subnet en format CIDR en adresse
     * masque.
     *
     * @Pre 32 >= subnet >= 0
     * @param subnet Valeu de la subnet à partir de laquelle on générera l'adresse
     * @return l'adresse masque propre à la subnet
     */
    public AdresseMasque convertirCIDREnMasque( int subnet )
    {
        int[] maskBits = new int[ 32 ];
        StringBuilder sb = new StringBuilder();
        int nbBit = 0;
        for ( int i = 0; i < maskBits.length; i++ )
        {
            if ( i < subnet )
            {
                maskBits[ i ] = 1;
            }
            nbBit++;
            sb.append( maskBits[ i ] );
            if ( nbBit % 8 == 0 )
            {
                sb.append( "." );
                nbBit = 0;
            }
        }
        String[] maskBitsStr = sb.toString().split( "\\." );
        for ( int i = 0; i < maskBitsStr.length; i++ )
        {
            maskBitsStr[ i ] = String.format( "%d", Integer.parseInt( maskBitsStr[ i ], 2 ) );
        }
        return new AdresseMasque( maskBitsStr );
    }

    /**
     * Fonction qui sert à calculer l'adresse sous-réseau ou
     * broadcast de sous-réseau.
     *
     * @Pre type == 0 ou type == 1
     * @param adresseMasque adresse à partir de laquelle nous ferons les calculs
     * @param type 0 si adresse sous-réseau, 1 si adresse broadcast de sous-réseau
     * @return l'adresse IP réseau ou broadcast
     */
    private AdresseIP calculSurMasqueDefault( AdresseMasque adresseMasque, String type )
    {
        String[] ipBin = getOctetsBinaire().split( "\\." );
        String[] masqueBin = adresseMasque.getOctetsBinaire().split( "\\." );
        String[] adresse = new String[ 4 ];

        for ( int i = 0; i < masqueBin.length; i++ )
        {
            StringBuilder octet = new StringBuilder();
            if ( Integer.parseInt( masqueBin[ i ], 2 ) == 255 )
            {
                for ( int j = 0; j < masqueBin[ i ].length(); j++ )
                {
                    octet.append( ipBin[ i ].charAt( j ) );
                }
            } else
            {
                for ( int j = 0; j < masqueBin[ i ].length(); j++ )
                {
                    octet.append( type );
                }
            }
            adresse[ i ] = octet.toString();
        }

        for ( int i = 0; i < adresse.length; i++ )
        {
            int octetDeci;
            octetDeci = Integer.parseInt( adresse[ i ], 2 );
            adresse[ i ] = Integer.toString( octetDeci );
        }

        return new AdresseIP( adresse );
    }

    /**
     * Fonction qui sert à calculer l'adresse réseau ou broadcast
     *
     * @Pre type == 0 ou type == 1
     * @param adresseMasque adresse à partir de laquelle nous ferons les calculs
     * @param type 0 si adresse réseau, 1 si adresse broadcast
     * @return l'adresse IP réseau ou broadcast
     */
    private AdresseIP calculSurMasque( AdresseMasque adresseMasque, String type )
    {
        String[] ipBin = getOctetsBinaire().split( "\\." );
        String[] masqueBin = adresseMasque.getOctetsBinaire().split( "\\." );
        String[] adresse = new String[ 4 ];

        for ( int j = 0; j < masqueBin.length; j++ )
        {
            StringBuilder octet = new StringBuilder();
            for ( int i = 0; i < masqueBin[ j ].length(); i++ )
            {
                if ( masqueBin[ j ].charAt( i ) == '1' )
                {
                    octet.append( ipBin[ j ].charAt( i ) );
                } else
                {
                    octet.append( type );
                }
            }
            adresse[ j ] = octet.toString();
        }

        for ( int i = 0; i < adresse.length; i++ )
        {
            int octetDeci;
            octetDeci = Integer.parseInt( adresse[ i ], 2 );
            adresse[ i ] = Integer.toString( octetDeci );
        }

        AdresseIP adresseIP = new AdresseIP( adresse );

        return adresseIP;
    }

    /**
     * Getter pour l'adresse de sous-réseau.
     *
     * @param adresseMasque masque à partir duquel nous ferons les calculs
     * @return l'adresse IP de sous-réseau
     */
    public AdresseIP getAdresseSousReseau( AdresseMasque adresseMasque )
    {
        return calculSurMasque( adresseMasque, "0" );
    }

    /**
     * Getter pour l'adresse de broadcast de sous-réseau.
     *
     * @param adresseMasque masque à partir duquel nous ferons les calculs
     * @return l'adresse IP de broadcast de sous-réseau
     */
    public AdresseIP getAdresseBroadcastSR( AdresseMasque adresseMasque )
    {
        return calculSurMasque( adresseMasque, "1" );
    }

    /**
     * Getter pour l'adresse IP broadcast.
     *
     * @param adresseMasque masque à partir duquel nous ferons les calculs
     * @return l'adresse IP de broadcast
     */
    public AdresseIP getAdresseBroadcast( AdresseMasque adresseMasque )
    {
        return calculSurMasqueDefault( adresseMasque, "1" );
    }

    /**
     * Getter pour l'adresse de réseau.
     *
     * @param adresseMasque masque à partir duquel nous ferons les calculs
     * @return l'adresse IP de réseau
     */
    public AdresseIP getAdresseReseau( AdresseMasque adresseMasque )
    {
        return calculSurMasqueDefault( adresseMasque, "0" );
    }

}
