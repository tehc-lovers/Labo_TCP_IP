package be.helha.tcpip.model;

/**
 * Classe qui sert à définir les deux fonctions principales du convertisseur.
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 * @see Convertisseur
 */
public interface IConversion
{
    /**
     * Fonction qui va convertir des octets décimaux en octets binaires.
     *
     * @param octetsDecimal tableau d'octets décimaux
     * @return un tableau d'octets en format binaire
     */
    String[] conversionBinaire( String[] octetsDecimal );

    /**
     * Fonction qui va convertir des octets binaires en octets décimaux.
     *
     * @param octetsBinaire tableau d'octets binaires
     * @return un tableau d'octets en format décimal
     */
    String[] conversionDecimal( String[] octetsBinaire );
}
