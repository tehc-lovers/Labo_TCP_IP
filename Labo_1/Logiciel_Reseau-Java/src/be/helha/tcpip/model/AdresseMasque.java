package be.helha.tcpip.model;

/**
 * Classe d'un masque.
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @see Adresse
 */
public class AdresseMasque extends Adresse
{
    /** Le complement binaire des octets d'un masque */
    private int[] adresseBitsComplement = null;

    /**
     * Constructeur d'un masque.
     * Dans ce cas si, un octet ne peut pas avoir une valeur plus petite que 0
     * ou plus grande que 255.
     *
     * @param octet1 le premier octet du masque
     * @param octet2 le deuxième octet du masque
     * @param octet3 le troisième octet du masque
     * @param octet4 le quatrième octet du masque
     * @Pre 255 >= octet1>= 0 && 255 >= octet2 >= 0 && 255 >= octet3 >= 0 && 255 >= octet4 >= 0
     * @Post octetsDecimal.length == 4
     */
    public AdresseMasque( String octet1, String octet2, String octet3, String octet4 )
    {
        super( octet1, octet2, octet3, octet4 );
        if( !( Integer.parseInt( octet1 ) > 255 || Integer.parseInt( octet2 ) > 255 || Integer.parseInt( octet3 ) > 255 || Integer.parseInt( octet4 ) > 255 ) )
        {
            adresseBitsComplement = getComplementOctetsBinairesAdresse();
        }
    }

    /**
     * Constructeur d'un masque via un tableau d'octets.
     *
     * @Pre bits.length == 4
     * @param bits tableau d'octets en format String
     */
    public AdresseMasque( String[] bits )
    {
        super( bits );
        adresseBitsComplement = getComplementOctetsBinairesAdresse();
    }

    /**
     * Fonction qui sert à récupérer le nombre d'hôtes dans une classe
     * d'adresse.
     *
     * @return le nombre d'hôtes
     */
    public int getNombreDeHotes()
    {
        int nbHosts = 0;
        for ( int i = adresseBitsComplement.length - 1; i >= 0; i-- )
        {
            if ( adresseBitsComplement[ i ] == 1 )
            {
                nbHosts += Math.pow( 2, adresseBitsComplement.length - i - 1 );
            }
        }
        return ++nbHosts;
    }

    /**
     * Fonction qui sert à récupérer le nombre de réseaux dans une classe
     * d'adresse.
     *
     * @return le nombre de réseaux
     */
    public int getNombreDeReseaux()
    {
        int nbReseaux = 0;
        int nbBits = 0;
        int nbMultipleDeHuit = 0;
        for ( int i = adresseBitsComplement.length - 1; i >= 0; i-- )
        {
            if ( adresseBitsComplement[ i ] == 0 )
            {
                nbBits++;
                if ( nbBits % 8 == 0 )
                {
                    nbMultipleDeHuit++;
                }
            }
        }
        nbReseaux += Math.pow( 2, nbBits - nbMultipleDeHuit );
        return nbReseaux;
    }
}
