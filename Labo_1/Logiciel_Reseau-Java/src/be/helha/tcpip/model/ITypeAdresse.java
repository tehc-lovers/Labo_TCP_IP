package be.helha.tcpip.model;

/**
 * Interface qui sert à instancier les différentes fonctions propres à
 * récupérer le type d'adresse.
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @see TypeAdresse
 */
public interface ITypeAdresse
{
    /**
     * Cette fonction sert à renvoyer une nouvelle instance du type d'adresse.
     *
     * @param adresseIP l'adresse IP dont nous souhaitons récupérer sa classe
     * @return la nouvelle instance propre au type de classe
     */
    TypeAdresse getType( AdresseIP adresseIP );

    /**
     * Fonction qui sert à renvoyer une chaîne de caractère propre à la
     * nouvelle instanciation de getType.
     *
     * @return le type de classe en format textuel
     */
    String getTypeStr();
}
