package be.helha.tcpip.model;

import java.util.ResourceBundle;

/**
 * Instanciation d'une adresse de classe indéterminée
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 * @see TypeAdresse
 */
public class TypeAdresseIndeterminee extends TypeAdresse
{
    /** Traducteur de chaînes de caractères */
    private ResourceBundle rb = ResourceBundle.getBundle( "be.helha.tcpip.res.strings" );

    /**
     * Constructeur pour le type d'adresse indéterminée avec le masque par défaut.
     */
    public TypeAdresseIndeterminee()
    {
        super( new AdresseMasque( "255", "255", "255", "255" ) );
    }

    /**
     * Fonction qui renvoie sa classe sous forme textuelle.
     *
     * @return "Indéterminée"
     */
    @Override
    public String getTypeStr()
    {
        return rb.getString( "string_no_network_class_determined" );
    }

    /**
     * Affichage textuel de la classe.
     * @return la classe sous forme de chaîne de caractères
     */
    @Override
    public String toString()
    {
        return String.format( "%s: %s\n",
                rb.getString( "string_type_of_address" ), rb.getString( "string_no_network_class_determined" ) );
    }
}
