package be.helha.tcpip.model;

import java.util.ResourceBundle;

/**
 * Classe qui sert principalement à renvoyer une sortie textuelle lors d'une
 * interaction.
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 */
public class Affichage
{
    /** Traducteur de chaînes de caractères */
    private final ResourceBundle rb;
    /** Interface qui définit le type d'adresse */
    private ITypeAdresse iTA = new TypeAdresseIndeterminee();

    /**
     * Constructeur de la classe Affichage
     * @param rb traducteur qui sert à afficher la chaîne de caractères propre à la langue de l'utilisateur
     */
    public Affichage( ResourceBundle rb )
    {
        this.rb = rb;
    }

    /**
     * Fonction qui sert à afficher textuellement le type d'adresse
     *
     * @param adresseIP adresse IP dont nous souhaitons récupérer la classe.
     * @return chaîne de caractères représentant le type d'adresse.
     */
    private String getTypeAdresseStr( AdresseIP adresseIP )
    {
        if ( iTA.getType( adresseIP ) instanceof TypeAdresseA )
        {
            iTA = new TypeAdresseA();
            return rb.getString( "string_type_of_address" ) + ": " + iTA.getTypeStr();
        } else if ( iTA.getType( adresseIP ) instanceof TypeAdresseB )
        {
            iTA = new TypeAdresseB();
            return rb.getString( "string_type_of_address" ) + ": " + iTA.getTypeStr();
        } else if ( iTA.getType( adresseIP ) instanceof TypeAdresseC )
        {
            iTA = new TypeAdresseC();
            return rb.getString( "string_type_of_address" ) + ": " + iTA.getTypeStr();
        } else if ( iTA.getType( adresseIP ) instanceof TypeAdresseIndeterminee )
        {
            iTA = new TypeAdresseIndeterminee();
            return rb.getString( "string_no_network_class_determined" );
        } else
        {
            return rb.getString( "string_invalid_components" );
        }
    }

    /**
     * Fonction qui sert à renvoyer un message textuel simple ou détaille
     * contenant la classe d'un réseau.
     *
     * @param adresseIP l'adresse IP dont nous souhaitons récupérer la classe
     * @param classFul si vrai, alors une sortie détaillée sera présentée
     * @return la chaîne de caractères contentant les informations sur la classe de l'adresse
     */
    public String getTypeStr( AdresseIP adresseIP, boolean classFul )
    {
        if ( classFul )
        {
            return iTA.getType( adresseIP ).toString();
        }
        return getTypeAdresseStr( adresseIP );
    }

    /**
     * Fonction qui sert à renvoyer une sortie textuelle pour récupérer l'adresse
     * réseau et broadcast d'une adresse IP et d'une adresse masque.
     *
     * @param adresseIP adresse IP dont nous souhaitons construire les informations
     * @param adresseMasque adresse masque qui caractérisera la découpe en sous-réseaux
     * @param sousReseau si vrai, alors une découpe en sous-réseaux a lieu
     * @return chaîne de caractère content l'adresse réseau et l'adresse de sous-réseau
     */
    public String getAdresseReseauEtBroadcast( AdresseIP adresseIP, AdresseMasque adresseMasque, boolean sousReseau )
    {
        StringBuilder sb = new StringBuilder();
        String adresseReseauStr = rb.getString( "string_network_address" );
        String adresseBroadcastStr = rb.getString( "string_broadcast_address" );
        AdresseIP adresseReseau = adresseIP.getAdresseReseau( adresseMasque );
        AdresseIP adresseBroadcast = adresseIP.getAdresseBroadcast( adresseMasque );
        if ( sousReseau )
        {
            String network = rb.getString( "string_network" );
            String subNetwork = rb.getString( "string_sub_network" );
            AdresseIP adresseSousReseau = adresseIP.getAdresseSousReseau( adresseMasque );
            AdresseIP adresseBroadcastSR = adresseIP.getAdresseBroadcastSR( adresseMasque );
            sb.append( String.format( "%40s %40s\n",
                    network, subNetwork ) );
            sb.append( String.format( "%-20s %-15s %30s\n", adresseReseauStr,
                    adresseReseau, adresseSousReseau ) );
            sb.append( String.format( "%-20s %-15s %30s\n", adresseBroadcastStr,
                    adresseBroadcast, adresseBroadcastSR ) );
            return sb.toString();
        }
        sb.append( String.format( "%-20s: %15s\n", adresseReseauStr, adresseReseau ) );
        sb.append( String.format( "%-20s: %15s\n", adresseBroadcastStr, adresseBroadcast ) );
        return sb.toString();
    }

    /**
     * Fonction qui sert à déterminer si deux adresses IP avec subnets différents feraient
     * partie ou non d'un même réseau.
     *
     * @param adresseIP1 l'adresse IP dans un réseau
     * @param adresseIP2 l'adresse IP dans un autre réseau
     * @param subnet1 la subnet de la première adresse IP
     * @param subnet2 la subnet de la deuxième adresse IP
     * @return retour qui confirme si les deux font partie d'un même réseau
     */
    private boolean faitPartieDuReseau( AdresseIP adresseIP1, AdresseIP adresseIP2, int subnet1, int subnet2 )
    {
        AdresseMasque adresseMasqueIP1 = adresseIP1.convertirCIDREnMasque( subnet1 );
        AdresseMasque adresseMasqueIP2 = adresseIP2.convertirCIDREnMasque( subnet2 );
        AdresseIP adresseBroadcastSR1 = adresseIP1.getAdresseBroadcastSR( adresseMasqueIP1 );
        AdresseIP adresseBroadcastSR2 = adresseIP2.getAdresseBroadcastSR( adresseMasqueIP2 );
        String[] adresseBroacastSR1Bits = adresseBroadcastSR1.getOctetsDecimal().split( "\\." );
        String[] adresseBroacastSR2Bits = adresseBroadcastSR2.getOctetsDecimal().split( "\\." );
        String[] adresseSousReseau1Bits = adresseIP1.getAdresseSousReseau( adresseMasqueIP1 ).getOctetsDecimal().split( "\\." );
        String[] adresseSousReseau2Bits = adresseIP2.getAdresseSousReseau( adresseMasqueIP2 ).getOctetsDecimal().split( "\\." );
        String[] adresseIP1Bits = adresseIP1.getOctetsDecimal().split( "\\." );
        String[] adresseIP2Bits = adresseIP2.getOctetsDecimal().split( "\\." );

        for ( int i = 0; i < adresseBroacastSR1Bits.length; i++ )
        {
            boolean ip1Verif1 = Integer.parseInt( adresseIP1Bits[ i ] ) > Integer.parseInt( adresseBroacastSR2Bits[ i ] );
            boolean ip1Verif2 = Integer.parseInt( adresseIP1Bits[ i ] ) < Integer.parseInt( adresseSousReseau2Bits[ i ] );
            boolean ip2Verif1 = Integer.parseInt( adresseIP2Bits[ i ] ) > Integer.parseInt( adresseBroacastSR1Bits[ i ] );
            boolean ip2Verif2 = Integer.parseInt( adresseIP2Bits[ i ] ) < Integer.parseInt( adresseSousReseau1Bits[ i ] );
            if ( ( ip1Verif1 || ip1Verif2 ) && ( ip2Verif1 || ip2Verif2 ) )
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Fonction qui sert à vérifier si deux adresses font partie du même réseau.
     *
     * @param adresseIP1 adresse IP du réseau
     * @param adresseIP2 adresse à vérifier
     * @param subnet subnet propre à l'adresse de réseau
     * @return sortie qui confirme si l'adresse IP fait effectivement partie du réseau
     */
    private boolean faitPartieDuReseau( AdresseIP adresseIP1, AdresseIP adresseIP2, int subnet )
    {
        AdresseMasque adresseMasqueIP = adresseIP1.convertirCIDREnMasque( subnet );
        AdresseIP adresseBroadcastSR = adresseIP1.getAdresseBroadcastSR( adresseMasqueIP );
        String[] adresseIP2Bits = adresseIP2.getOctetsDecimal().split( "\\." );
        String[] adresseBroacastSRBits = adresseBroadcastSR.getOctetsDecimal().split( "\\." );
        String[] adresseSousReseauBits = adresseIP1.getAdresseSousReseau( adresseMasqueIP ).getOctetsDecimal().split( "\\." );

        for ( int i = 0; i < adresseBroacastSRBits.length; i++ )
        {
            boolean verif1 = Integer.parseInt( adresseIP2Bits[ i ] ) > Integer.parseInt( adresseBroacastSRBits[ i ] );
            boolean verif2 = Integer.parseInt( adresseIP2Bits[ i ] ) < Integer.parseInt( adresseSousReseauBits[ i ] );
            if ( verif1 || verif2 )
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Fonction qui sert à vérifier si deux adresses machines font partie du même réseau.
     *
     * @param adresseIP1 adresse IP machine
     * @param adresseIP2 adresse à vérifier
     * @param subnet subnet propre à l'adresse de réseau
     * @return sortie qui confirme si l'adresse IP machine fait effectivement partie du réseau
     */
    private boolean faitPartieDuReseauMachine( AdresseIP adresseIP1, AdresseIP adresseIP2, int subnet )
    {
        AdresseMasque adresseMasqueIP = adresseIP1.convertirCIDREnMasque( subnet );
        AdresseIP adresseBroadcastSR = adresseIP1.getAdresseBroadcastSR( adresseMasqueIP );
        String[] adresseIP2Bits = adresseIP2.getOctetsDecimal().split( "\\." );
        String[] adresseBroacastSRBits = adresseBroadcastSR.getOctetsDecimal().split( "\\." );
        String[] adresseSousReseauBits = adresseIP1.getAdresseSousReseau( adresseMasqueIP ).getOctetsDecimal().split( "\\." );

        for ( int i = 0; i < adresseBroacastSRBits.length; i++ )
        {
            boolean verif1 = Integer.parseInt( adresseIP2Bits[ i ] ) > Integer.parseInt( adresseBroacastSRBits[ i ] );
            boolean verif2 = Integer.parseInt( adresseIP2Bits[ i ] ) < Integer.parseInt( adresseSousReseauBits[ i ] );
            if ( verif1 || verif2 )
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Fonction qui sert à déterminer si deux adresses IP avec subnets différents feraient
     * partie ou non d'un même réseau.
     *
     * @param adresseIP1 l'adresse IP dans un réseau
     * @param adresseIP2 l'adresse IP dans un autre réseau
     * @param subnet1 la subnet de la première adresse IP
     * @param subnet2 la subnet de la deuxième adresse IP
     * @return retour textuel qui confirme si les deux font partie d'un même réseau
     */
    public String faitPartieDuReseauStr( AdresseIP adresseIP1, AdresseIP adresseIP2, int subnet1, int subnet2 )
    {
        AdresseMasque adresseMasque;
        AdresseIP adresseReseau;
        int subnet;
        StringBuilder sb = new StringBuilder();
        sb.append( String.format( "%s: %s/%d\n", rb.getString( "string_ip_address" ), adresseIP1, subnet1 ) );
        sb.append( String.format( "%s: %s/%d\n", rb.getString( "string_ip_address" ), adresseIP2, subnet2 ) );
        if ( subnet1 < subnet2 )
        {
            adresseMasque = adresseIP1.convertirCIDREnMasque( subnet1 );
            adresseReseau = adresseIP1.getAdresseReseau( adresseMasque );
            subnet = subnet1;
        } else if ( subnet2 < subnet1 )
        {
            adresseMasque = adresseIP2.convertirCIDREnMasque( subnet2 );
            adresseReseau = adresseIP2.getAdresseReseau( adresseMasque );
            subnet = subnet2;
        } else
        {
            adresseMasque = adresseIP1.convertirCIDREnMasque( subnet1 );
            adresseReseau = adresseIP1.getAdresseReseau( adresseMasque );
            subnet = subnet1;
        }
        if ( faitPartieDuReseau( adresseIP1, adresseIP2, subnet1, subnet2 ) )
        {
            sb.append( String.format( "%s: %s - /%d",
                    rb.getString( "string_are_part_of_the_same_network" ), adresseMasque, subnet ) );
            return sb.toString();
        }
        sb.append( String.format( "%s %s - /%d",
                rb.getString( "string_are_not_part_of_the_same_network" ), adresseMasque, subnet ) );
        return sb.toString();
    }

    /**
     * Fonction qui sert à vérifier si deux adresses font partie du même réseau.
     *
     * @param adresseIP1 adresse IP du réseau
     * @param adresseIP2 adresse à vérifier
     * @param subnet subnet propre à l'adresse de réseau
     * @return sortie textuelle qui confirme si l'adresse IP fait effectivement partie du réseau
     */
    public String faitPartieDuReseauStr( AdresseIP adresseIP1, AdresseIP adresseIP2, int subnet )
    {
        AdresseMasque adresseMasque = adresseIP1.convertirCIDREnMasque( subnet );
        AdresseIP adresseReseau = adresseIP1.getAdresseSousReseau( adresseMasque );
        StringBuilder sb = new StringBuilder();
        sb.append( String.format( "%s: %s ", rb.getString( "string_ip_address" ), adresseIP2 ) );
        if ( faitPartieDuReseau( adresseIP1, adresseIP2, subnet ) )
        {
            sb.append( String.format( "%s %s/%d",
                    rb.getString( "string_is_part_of_network" ), adresseReseau, subnet ) );
            return sb.toString();
        }
        sb.append( String.format( "%s %s/%d", rb.getString( "string_is_not_part_of_network" ), adresseReseau, subnet ) );
        return sb.toString();
    }

    /**
     * Fonction qui sert à vérifier si deux adresses machines font partie du même réseau.
     *
     * @param adresseIP1 adresse IP du réseau
     * @param adresseIP2 adresse à vérifier
     * @param subnet subnet propre à l'adresse de réseau
     * @return sortie textuelle qui confirme si l'adresse IP fait effectivement partie du réseau
     */
    public String faitPartieDuReseauMachineStr( AdresseIP adresseIP1, AdresseIP adresseIP2, int subnet )
    {
        AdresseMasque adresseMasque = adresseIP1.convertirCIDREnMasque( subnet );
        AdresseIP adresseReseau = adresseIP1.getAdresseSousReseau( adresseMasque );
        AdresseMasque adresseMasqueIP = adresseIP1.convertirCIDREnMasque( subnet );
        AdresseIP adresseBroadcastSR = adresseIP1.getAdresseBroadcastSR( adresseMasqueIP );
        boolean verif1 = adresseIP1.getOctetsDecimal().equalsIgnoreCase( adresseBroadcastSR.getOctetsDecimal() );
        boolean verif2 = adresseIP1.getOctetsDecimal().equalsIgnoreCase( adresseIP1.getAdresseSousReseau( adresseMasqueIP ).getOctetsDecimal() );
        boolean verif3 = adresseIP2.getOctetsDecimal().equalsIgnoreCase( adresseBroadcastSR.getOctetsDecimal() );
        boolean verif4 = adresseIP2.getOctetsDecimal().equalsIgnoreCase( adresseIP1.getAdresseSousReseau( adresseMasqueIP ).getOctetsDecimal() );
        if( verif1 || verif2 || verif3 || verif4 )
        {
            return rb.getString( "string_not_a_machine_address" );
        }
        StringBuilder sb = new StringBuilder();
        sb.append( String.format( "%s: %s ", rb.getString( "string_ip_address" ), adresseIP2 ) );
        if ( faitPartieDuReseauMachine( adresseIP1, adresseIP2, subnet ) )
        {
            sb.append( String.format( "%s %s/%d",
                    rb.getString( "string_is_part_of_network" ), adresseReseau, subnet ) );
            return sb.toString();
        }
        sb.append( String.format( "%s %s/%d", rb.getString( "string_is_not_part_of_network" ), adresseReseau, subnet ) );
        return sb.toString();
    }
}
