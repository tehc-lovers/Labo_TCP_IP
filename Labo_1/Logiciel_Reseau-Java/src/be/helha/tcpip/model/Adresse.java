package be.helha.tcpip.model;

/**
 * Classe parent de tout type d'adresse: - Adresse IP - Adresse Masque
 * <p>
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 */
public abstract class Adresse
{
    /** Le convertisseur de binaire en décimal et vice versa */
    private final IConversion convertisseur = new Convertisseur();
    /** Les octets décimaux propres à l'adresse */
    private String[] octetsDecimal = new String[ 4 ];
    /** Les octets binaires propres à l'adresse */
    private String[] octetsBinaire = new String[ 4 ];

    /**
     * Une adresse est composée de 4 octets.
     *
     * @param octet1 le premier octet
     * @param octet2 le deuxième octet
     * @param octet3 le troisième octet
     * @param octet4 le quatrième octet
     */
    public Adresse( String octet1, String octet2, String octet3, String octet4 )
    {
        this.octetsDecimal[ 0 ] = octet1;
        this.octetsDecimal[ 1 ] = octet2;
        this.octetsDecimal[ 2 ] = octet3;
        this.octetsDecimal[ 3 ] = octet4;
        convertirEnBinaire();
    }

    /**
     * Il est tout autant possible d'instancier une adresse via un tableau
     * content les différents octets.
     *
     * @param octets le tableau d'octets qui characterise l'adresse.
     */
    public Adresse( String[] octets )
    {
        this( octets[ 0 ], octets[ 1 ], octets[ 2 ], octets[ 3 ] );
    }

    /**
     * Fonction qui sert à convertir des octets décimaux en octets binaires.
     *
     * @Post octetsBinaire.length == 4
     */
    private void convertirEnBinaire()
    {
        octetsBinaire = convertisseur.conversionBinaire( octetsDecimal );
    }

    /**
     * Fonction qui sert à convertir des octets binaires en octets décimaux.
     *
     * @Post octetsDecimal.length == 4
     */
    private void convertirEnDecimal()
    {
        octetsDecimal = convertisseur.conversionDecimal( octetsBinaire );
    }

    /**
     * Getter des octets décimaux en format chaîne de caractères.
     *
     * @return une chaîne de caractères au format suivant format: 192.168.1.1
     */
    public String getOctetsDecimal()
    {
        return String.join( ".", octetsDecimal );
    }

    /**
     * Getter des octets binaires en format chaîne de caractères.
     *
     * @return une chaîne de caractères du style: 11111111.11110000
     */
    public String getOctetsBinaire()
    {
        return String.join( ".", octetsBinaire );
    }

    /**
     * Getter des octets binaires en format tableau de int. Utilisé
     * principalement pour les calculs mathématiques.
     *
     * @return les octets décimaux de l'adresse sous forme d'un tableau de int
     */
    public int[] getOctetsAdresse()
    {
        int[] ipBits = new int[ 4 ];
        String[] ipBitsStr = getOctetsDecimal().split( "\\." );
        int nbBit = 0;
        for ( String s : ipBitsStr )
        {
            ipBits[ nbBit ] = Integer.parseInt( s );
            nbBit++;
        }
        return ipBits;
    }

    /**
     * Getter des octets binaires en format tableau de int. Utilisé
     * principalement pour les calculs mathématiques.
     *
     * @return les binaires octets de l'adresse sous forme d'un tableau de int
     */
    public int[] getOctetsBinairesAdresse()
    {
        int[] adresseBinaryBits = new int[ 32 ];
        String[] maskBitsStr = getOctetsBinaire().split( "\\." );

        int nbBit = 0;
        for ( String s : maskBitsStr )
        {
            for ( int i = 0; i < s.length(); i++ )
            {
                adresseBinaryBits[ nbBit ] = Integer.parseInt( String.format( "%c", s.charAt( i ) ) );
                nbBit++;
            }
        }
        return adresseBinaryBits;
    }

    /**
     * Getter du complement des octets binaires en format tableau de int.
     * Utilisé principalement pour les calculs mathématiques.
     *
     * @return le complement des binaires octets de l'adresse sous forme d'un
     * tableau de int
     */
    public int[] getComplementOctetsBinairesAdresse()
    {
        int[] adresseBinaryBitsComplement = getOctetsBinairesAdresse();
        for ( int i = 0; i < adresseBinaryBitsComplement.length; i++ )
        {
            int bit = -1;
            switch ( adresseBinaryBitsComplement[ i ] )
            {
                case 0:
                    bit = 1;
                    break;
                case 1:
                    bit = 0;
                    break;
                default:
                    break;
            }
            adresseBinaryBitsComplement[ i ] = bit;
        }
        return adresseBinaryBitsComplement;
    }

    /**
     * Fonction qui renvoie l'adresse sous forme de chaîne de caractères
     * content les octets décimaux.
     *
     * @return les octets décimaux de l'adresse
     */
    public String toString()
    {
        return getOctetsDecimal();
    }
}
