package be.helha.tcpip.test;

import be.helha.tcpip.model.AdresseIP;
import be.helha.tcpip.model.ITypeAdresse;
import be.helha.tcpip.model.TypeAdresseIndeterminee;
import be.helha.tcpip.model.Validator;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Classe pour les tests unitaires.
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 */
public class ValidatorTests
{
    /** Le validator qui vérifier l'input de l'utilisateur */
    private static Validator validator;
    /** Le traducteur de chaînes de caractères */
    private static ResourceBundle rb;
    /** Adresse IP à tester */
    private static AdresseIP ipValid;
    /** Instanciation du type d'adresse */
    private static ITypeAdresse iTypeAdresse;

    /**
     * Fonction qui instancie toutes les variables avant d'exécuter tout test
     * unitaire.
     */
    @BeforeAll
    static void initAll()
    {
        rb = ResourceBundle.getBundle( "be.helha.tcpip.res.strings" );
        validator = new Validator( rb );
        ipValid = new AdresseIP( "192", "168", "1", "1" );
        iTypeAdresse = new TypeAdresseIndeterminee();
    }

    /**
     * Fonction qui détruit tous les objets instanciés lors de la fin des
     * tests unitaires.
     */
    @AfterAll
    static void tearDownAll()
    {
        validator = null;
        rb = null;
    }

    /**
     * Fonction qui fait des nouvelles instaciations lorsqu'un nouveau test
     * est exécuté.
     */
    @BeforeEach
    void init()
    {
    }

    /**
     * Test unitaire sur une adresse IP valide.
     */
    @Test
    public void ajouterIpValideTest()
    {
        String correctOutput = String.format( "%s: %s", rb.getString( "string_type_of_address" ), iTypeAdresse.getType( ipValid ).getTypeStr() );
        assertEquals( correctOutput, validator.validateFonction1( ipValid, false ), rb.getString( "string_invalid_address" ) );
    }

    /**
     * Fonction qui détruit toute instanciation après chaque test unitaire.
     */
    @AfterEach
    void tearDown()
    {
    }
}
