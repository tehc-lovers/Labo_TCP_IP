package be.helha.tcpip;

import be.helha.tcpip.gui.FrameMain;

import javax.swing.SwingUtilities;

/**
 * Point de départ de l'application.
 *
 * @author Caculli Giorgio
 * @author Lambert Guillaume
 * @author Taminiau Tanguy
 * @version 1.0
 */
public class Main
{
    /**
     * La fonction main appelée en premier lors du lancement du logiciel.
     * Dès le lancement du logiciel, une des premières choses qui seront
     * chargé sera d'ouvrir la fenêtre.
     *
     * @param args les différents arguments pour intéragir avec le logiciel
     */
    public static void main( String[] args )
    {
        SwingUtilities.invokeLater( new Runnable()
        {
            @Override
            public void run()
            {
                new FrameMain();
            }
        } );
    }
}
