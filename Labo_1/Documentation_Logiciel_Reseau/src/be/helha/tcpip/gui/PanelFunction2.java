package be.helha.tcpip.gui;

import be.helha.tcpip.model.AdresseIP;
import be.helha.tcpip.model.AdresseMasque;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

/**
 * Panneau pour la fonction 2
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 * @see PanelFunctionBase
 */
public class PanelFunction2 extends PanelFunctionBase
{
    /** Affichage textuel lors d'une action */
    private final JTextArea outputTextArea = new JTextArea();

    /**
     * Constructeur pour la fonction 2.
     *
     * @param rb le traducteur de chaînes de caractères
     */
    public PanelFunction2( ResourceBundle rb )
    {
        super( rb );

        JPanel centerPanel = new JPanel( new GridLayout( 2, 1 ) );
        centerPanel.add( panelInput( rb ) );
        centerPanel.add( panelOutput( rb ) );
        centerPanel.setBackground( backgroundColor );

        add( centerPanel, BorderLayout.CENTER );
    }

    /**
     * Constructeur pour le panneau dont l'utilisateur pourra introduit son
     * input.
     *
     * @param rb le traducteur de chaînes de caractères
     * @return Le panneau contenant les inputs
     */
    private JPanel panelInput( ResourceBundle rb )
    {
        JPanel tmpPanelIPInput = new JPanel( new GridBagLayout() );
        tmpPanelIPInput.setBackground( backgroundColor );
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets( INSET_TOP, INSET_LEFT, INSET_BOTTOM, INSET_RIGHT );
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 0.5;

        JTextField[] ipBits = new JTextField[ 4 ];
        JTextField[] macBits = new JTextField[ 4 ];
        int pos = 0;
        for ( int i = 0; i < 7; i++ )
        {
            if ( i % 2 == 0 )
            {
                ipBits[ pos ] = new JTextField( MAX_CHARACTERS_IPV4_BITS );
                ipBits[ pos ].setFont( font );
                ipBits[ pos ].setHorizontalAlignment( JTextField.CENTER );
                AbstractDocument doc = ( AbstractDocument ) ipBits[ pos ].getDocument();
                doc.setDocumentFilter( filterIPV4 );
                doc.addDocumentListener( documentListenerIPV4 );
                c.gridx = i;
                c.gridy = 1;
                tmpPanelIPInput.add( ipBits[ pos ], c );
            } else
            {
                pos += 1;
                JLabel dotLabel = new JLabel( "." );
                dotLabel.setFont( font );
                dotLabel.setHorizontalAlignment( JLabel.CENTER );
                c.gridx = i;
                c.gridy = 1;
                tmpPanelIPInput.add( dotLabel, c );
            }
        }
        pos = 0;
        for ( int j = 0; j < 7; j++ )
        {
            if ( j % 2 == 0 )
            {
                macBits[ pos ] = new JTextField( MAX_CHARACTERS_IPV4_BITS );
                macBits[ pos ].setFont( font );
                macBits[ pos ].setHorizontalAlignment( JTextField.CENTER );
                AbstractDocument doc = ( AbstractDocument ) macBits[ pos ].getDocument();
                doc.setDocumentFilter( filterIPV4 );
                doc.addDocumentListener( documentListenerIPV4 );
                c.gridx = j;
                c.gridy = 3;
                tmpPanelIPInput.add( macBits[ pos ], c );
            } else
            {
                pos += 1;
                JLabel dotLabel = new JLabel( "." );
                dotLabel.setFont( font );
                dotLabel.setHorizontalAlignment( JLabel.CENTER );
                c.gridx = j;
                c.gridy = 3;
                tmpPanelIPInput.add( dotLabel, c );
            }
        }

        JLabel labelIP = new JLabel( rb.getString( "string_ip_address" ) );
        labelIP.setFont( font );
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = MAX_LENGTH_IPV4_STRING;
        tmpPanelIPInput.add( labelIP, c );

        JLabel labelIP2 = new JLabel( "Masque" );
        labelIP2.setFont( font );
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = MAX_LENGTH_IPV4_STRING;
        tmpPanelIPInput.add( labelIP2, c );

        JButton buttonSubmit = new JButton( rb.getString( "button_submit" ) );
        buttonSubmit.addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                String ipBit1 = ipBits[ 0 ].getText();
                String ipBit2 = ipBits[ 1 ].getText();
                String ipBit3 = ipBits[ 2 ].getText();
                String ipBit4 = ipBits[ 3 ].getText();
                String macBit1 = macBits[ 0 ].getText();
                String macBit2 = macBits[ 1 ].getText();
                String macBit3 = macBits[ 2 ].getText();
                String macBit4 = macBits[ 3 ].getText();
                String outputString = getValidator().validateFonction2(
                        new AdresseIP( ipBit1, ipBit2, ipBit3, ipBit4 ),
                        new AdresseMasque( macBit1, macBit2, macBit3, macBit4 )
                );
                outputTextArea.setText( outputString );
            }
        } );
        buttonSubmit.setFont( font );
        c.gridx = 0;
        c.gridy = 4;
        c.gridwidth = MAX_LENGTH_IPV4_STRING;
        tmpPanelIPInput.add( buttonSubmit, c );

        return tmpPanelIPInput;
    }

    /**
     * Fonction qui sert à créer le panneau contentant la réponse suite à
     * l'input introduit par l'utilisateur.
     *
     * @param rb traducteur de chaînes de caractères
     * @return un panneau contenant la TextArea
     */
    private JPanel panelOutput( ResourceBundle rb )
    {
        JPanel tmpPanelOutput = new JPanel( new GridBagLayout() );
        tmpPanelOutput.setBackground( backgroundColor );
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets( INSET_TOP, INSET_LEFT, INSET_BOTTOM, INSET_RIGHT );
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 0.5;

        outputTextArea.setWrapStyleWord( true );
        outputTextArea.setLineWrap( true );
        outputTextArea.setEditable( false );
        outputTextArea.setFocusable( false );
        outputTextArea.setOpaque( false );
        outputTextArea.setFont( font );
        outputTextArea.setBackground( backgroundColor );
        tmpPanelOutput.add( outputTextArea, c );

        return tmpPanelOutput;
    }
}
