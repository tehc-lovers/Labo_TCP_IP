package be.helha.tcpip.gui;

import be.helha.tcpip.Main;

import javax.swing.JFrame;
import java.awt.Image;
import java.awt.Taskbar;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * <p>
 * Frame principale de l'application. Charge automatiquement les différents
 * panel qui seront stockés dans un CardLayout.
 * <p>
 * Le WindowListener sert à vérifier l'état de la fenêtre, soit: - Ouverte -
 * Fermée - En train de s'ouvrir - En train de se fermer - Si elle vient d'être
 * "icônifiée" - Si elle vient de sortir de l'état "icônifiée" - Activée -
 * Désactivée
 * <p>
 * Le WindowFocusListener sert à vérifier si l'utilisateur est bel est bien sur
 * la fenêtre ou s'il l'a mise côté.
 * <p>
 * Le WindowStateListener sert à vérifier s'il y a eu un quelconque changement
 * d'état de la fenêtre.
 *
 * @author Caculli Giorgio
 * @author Lambert Guillaume
 * @author Taminiau Tanguy
 * @version 1.0
 * @see JFrame
 * @see WindowListener
 * @see WindowFocusListener
 * @see WindowStateListener
 */
public class FrameMain extends JFrame
        implements WindowListener, WindowFocusListener, WindowStateListener,
        IDisposable
{
    private static final short WIDTH = 1024;
    private static final short HEIGHT = WIDTH / 4 * 3;
    /**
     * Resource Bundle qui sert à charger les différentes chaînes de caractère
     * dépendamment du langage du système.
     */
    private final ResourceBundle rb = ResourceBundle.getBundle( "be.helha.tcpip.res.strings" );

    /**
     * Toujours devant les fenêtres.
     * <p>
     * DO_NOTHING_ON_CLOSE est mit pour permettr à l'utilisateur de confirmer le
     * fait qu'il veut quitter le logiciel.
     * <p>
     * La fenêtre n'a pas besoin de changer de taille, donc resizable = false.
     * <p>
     * Taille par défaut 1024x768 - 4:3 Le titre changera dépendamment du
     * langage du système.
     * <p>
     * Automatiquement visible.
     * <p>
     * Focalisée dès son ouverture.
     */
    public FrameMain()
    {
        URL iconURL = Main.class.getResource( "assets/icon-poe.png" );
        Toolkit kit = Toolkit.getDefaultToolkit();
        Image img = kit.createImage( iconURL );
        setIconImage( img );

        try
        {
            final Taskbar taskbar = Taskbar.getTaskbar();
            taskbar.setIconImage( img );
        } catch ( UnsupportedOperationException | SecurityException e )
        {
            e.printStackTrace();
        }

        setAlwaysOnTop( true );
        setDefaultCloseOperation( DO_NOTHING_ON_CLOSE );
        setJMenuBar( new TopMenuBar( rb ) );
        setLocationByPlatform( true );
        setResizable( false );
        setSize( WIDTH, HEIGHT );
        setTitle( rb.getString( "title" ) );
        setVisible( true );

        requestFocus( true );

        addWindowListener( this );
        addWindowFocusListener( this );
        addWindowStateListener( this );
        addKeyListener( new KeyHandler() );
        add( new PanelMain( rb ) );
    }

    /**
     * Cette fonction sert à faire une action lorsque la fenêtre vient d'être
     * focalisée.
     *
     * @param event le type d'event avec lequel on souhaite interagir
     */
    @Override
    public void windowGainedFocus( WindowEvent event )
    {
    }

    /**
     * Cette fonction sert à faire une action lorsque la fenêtre vient de perdre
     * sa focalisation.
     *
     * @param event le type d'event avec lequel on souhaite interagir
     */
    @Override
    public void windowLostFocus( WindowEvent event )
    {
    }

    /**
     * Cette fonction sert à faire une action lorsque la fenêtre est ouverte.
     *
     * @param event le type d'event avec lequel on souhaite interagir
     */
    @Override
    public void windowOpened( WindowEvent event )
    {
    }

    /**
     * Cette fonction sert à faire une action lorsque la fenêtre est en cours de
     * fermeture.
     *
     * @param event le type d'event avec lequel on souhaite interagir
     */
    @Override
    public void windowClosing( WindowEvent event )
    {
        disposeWindowConfirm( getRootPane(), rb );
    }

    /**
     * Cette fonction sert à faire une action lorsque la fenêtre est fermée.
     *
     * @param event le type d'event avec lequel on souhaite interagir
     */
    @Override
    public void windowClosed( WindowEvent event )
    {
    }

    /**
     * Cette fonction sert à faire une action lorsque la fenêtre est réduite en
     * icône.
     *
     * @param event le type d'event avec lequel on souhaite interagir
     */
    @Override
    public void windowIconified( WindowEvent event )
    {
    }

    /**
     * Cette fonction sert à faire une action lorsque la fenêtre est sortie de
     * son état d'icône.
     *
     * @param event le type d'event avec lequel on souhaite interagir
     */
    @Override
    public void windowDeiconified( WindowEvent event )
    {
    }

    /**
     * Cette fonction sert à faire une action lorsque la fenêtre vient d'être
     * activée.
     *
     * @param event le type d'event avec lequel on souhaite interagir
     */
    @Override
    public void windowActivated( WindowEvent event )
    {
    }

    /**
     * Cette fonction sert à faire une action lorsque la fenêtre vient d'être
     * désactivée.
     *
     * @param event le type d'event avec lequel on souhaite interagir
     */
    @Override
    public void windowDeactivated( WindowEvent event )
    {
    }

    /**
     * Cette fonction sert à faire une action lorsque l'état de la fenêtre a été
     * changé.
     *
     * @param event le type d'event avec lequel on souhaite interagir
     */
    @Override
    public void windowStateChanged( WindowEvent event )
    {
    }

    /**
     * Lors de l'ouverture de la fenêtre, on va permettre à l'utilisateur de
     * pouvoir utiliser des touches pour faire certaines actions, comme par
     * exemple: - Appuyer sur ESCAPE pour demander de quitter le logiciel.
     */
    private class KeyHandler implements KeyListener
    {

        /**
         * Fonction qui a lieu lorsqu'une touche du clavier a bien été appuyé et
         * relâchée.
         *
         * @param event le type d'event avec lequel on souhaite interagir
         */
        @Override
        public void keyTyped( KeyEvent event )
        {
        }

        /**
         * Fonction qui a lieu que lorsqu'une touche est appuyée.
         *
         * @param event le type d'event avec lequel on souhaite interagir
         */
        @Override
        public void keyPressed( KeyEvent event )
        {
        }

        /**
         * Fonction qui a lieu lorsqu'une touche est relâchée.
         *
         * @param event le type d'event avec lequel on souhaite interagir
         */
        @Override
        public void keyReleased( KeyEvent event )
        {
            int source = event.getKeyCode();
            switch ( source )
            {
                case KeyEvent.VK_ESCAPE:
                    disposeWindowConfirm( getRootPane(), rb );
                    break;
                case KeyEvent.VK_F1:
                    break;
            }
        }
    }
}
