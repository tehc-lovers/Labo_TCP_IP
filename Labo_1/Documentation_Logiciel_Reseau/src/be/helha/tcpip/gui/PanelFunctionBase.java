package be.helha.tcpip.gui;

import be.helha.tcpip.model.Validator;

import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.ResourceBundle;

/**
 * Panneau qui sert de design de base pour les autres panneaux.
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 * @see PanelFunctionBase
 */
public abstract class PanelFunctionBase extends JPanel implements GUIDefaultValues, ICardHandler
{
    /** Validator de base qui valide tout input fait par l'utilisateur */
    private Validator validator = null;
    /** Traducteur de chaînes de caractères */
    private ResourceBundle rb = null;
    /** Bouton qui renvoie l'utilisateur vers le menu principal */
    private JButton backToMainMenuButton = null;

    /**
     * Constructeur pour la base des différents panneaux
     *
     * @param rb traducteur de chaînes de caractères
     */
    public PanelFunctionBase( ResourceBundle rb )
    {
        this.rb = rb;
        validator = new Validator( rb );

        initBackToMainMenuButton();

        setLayout( new BorderLayout() );
        setBackground( backgroundColor );

        add( backToMainMenuButton, BorderLayout.PAGE_END );
    }

    /**
     * Fonction qui sert à initialiser le bouton pour le retour vers le menu
     * principal.
     */
    public void initBackToMainMenuButton()
    {
        backToMainMenuButton = new JButton( rb.getString( "button_back_to_main_menu" ) );
        backToMainMenuButton.setFont( font );
        backToMainMenuButton.addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                showCard( getParent(), "panel.menu" );
            }
        } );
    }

    /**
     * Getter sur le validator.
     *
     * @return le validator initialisé
     */
    public Validator getValidator()
    {
        return validator;
    }

    /**
     * Interface qui sert à instancier une instance de AutoFocusTransferDelegate.
     */
    protected interface AutoFocusTransferDelegate
    {
        boolean shouldTransferFocus( Document d );
    }

    /**
     * Interface qui sert à instancier une instance de AutoFocusTransferListener
     * avec la fonction appropriée.
     */
    protected interface AutoFocusTransferListener
    {
        void focusTransferShouldOccur( Document d );
    }

    /**
     * Classe dont la fonction sert à vérifier si l'utilisateur a bien input
     * 3 caractères maximum, si oui, alors cette classe va transférer
     * l'utilisateur vers le prochain input automatiquement.
     *
     * @author Giorgio Caculli
     * @author Guillaume Lambert
     * @author Tanguy Taminiau
     * @version 1.0
     */
    protected static class MaxLengthAutoFocusTransferDelegate implements AutoFocusTransferDelegate
    {
        /** La nombre maximale de caractères */
        private final int maxLength;

        /**
         * Constructeur de la classe qui impose la longueur maximale.
         *
         * @param maxLength la longueur maximale de caractères acceptés
         */
        public MaxLengthAutoFocusTransferDelegate( int maxLength )
        {
            this.maxLength = maxLength;
        }

        /**
         * Fonction qui renvoie le focus vers un autre composant.
         *
         * @param d le document à écouter
         * @return vrai si le nombre maximum de caractères a été introduit
         */
        @Override
        public boolean shouldTransferFocus( Document d )
        {
            return d.getLength() >= maxLength;
        }
    }

    /**
     * Classe qui sert à imposer une limite aux inputs faits par l'utilisateur.
     *
     * @author Giorgio Caculli
     * @author Guillaume Lambert
     * @author Tanguy Taminiau
     * @version 1.0
     * @see DocumentFilter
     */
    protected static class JTextFieldLimit extends DocumentFilter
    {
        /** Le nombre maximum de caractères qui sont acceptés dans le textfield */
        private final int limit;

        /**
         * Constructeur de ce limiteur.
         *
         * @param limit la nombre maximal de caractères a accepter
         */
        public JTextFieldLimit( int limit )
        {
            this.limit = limit;
        }

        /**
         * Fonction qui sert à montrer l'input de l'utilisateur.
         *
         * @param filterBypass filtre qui arrête l'input lorsque la limite est atteinte
         * @param length la longueur maximale
         * @param string la chaîne de caractères à vérifier
         * @param attributeSet attribut qui detecte la nature de l'input
         * @throws BadLocationException exception qui averti lorsqu'une interaction a lieu où elle ne devrait pas avoir lieu
         */
        public void insertString( FilterBypass filterBypass, int length, String string, AttributeSet attributeSet ) throws BadLocationException
        {
            if ( string == null )
            {
                return;
            }
            if ( ( filterBypass.getDocument().getLength() + string.length() - length ) <= limit )
            {
                super.insertString( filterBypass, length, string, attributeSet );
            }
        }

        /**
         * Fonction qui sert à remplacer l'input de l'utilisateur.
         *
         * @param filterBypass filtre qui arrête l'input lorsque la limite est atteinte
         * @param offset paramètre qui définit le range que l'on souhaite remplacer
         * @param length la longueur maximale
         * @param string la chaîne de caractères à vérifier
         * @param attributeSet attribut qui detecte la nature de l'input
         * @throws BadLocationException exception qui averti lorsqu'une interaction a lieu où elle ne devrait pas avoir lieu
         */
        public void replace( FilterBypass filterBypass, int offset, int length, String string, AttributeSet attributeSet ) throws BadLocationException
        {
            if ( string == null )
            {
                return;
            }
            if ( ( filterBypass.getDocument().getLength() + string.length() - length ) <= limit )
            {
                super.replace( filterBypass, offset, length, string, attributeSet );
            }
        }
    }

    /**
     * Classe qui va gérer les transferts d'un component à un autre pendant
     * les inputs faits par l'utilisateur.
     *
     * @author Giorgio Caculli
     * @author Guillaume Lambert
     * @author Tanguy Taminiau
     * @version 1.0
     * @see DocumentListener
     */
    protected static class AutoFocusTransferHandler implements DocumentListener
    {
        /** Instance locale d'un listener */
        private final AutoFocusTransferListener listener;
        /** Instance locale d'un delegate */
        private final AutoFocusTransferDelegate delegate;

        /**
         * Constructeur de ce focus handler.
         *
         * @param listener le listener qui écoutera l'input
         * @param delegate le delegate qui enverra l'input vers un autre component
         */
        public AutoFocusTransferHandler( AutoFocusTransferListener listener, AutoFocusTransferDelegate delegate )
        {
            this.listener = listener;
            this.delegate = delegate;
        }

        /**
         * Fonction qui met à jours le component lors des insertions.
         *
         * @param e les insertions qui ont lieu dans le TextField
         */
        @Override
        public void insertUpdate( DocumentEvent e )
        {
            checkForTransfer( e.getDocument() );
        }

        /**
         * Fonction qui met à jours le component lors des suppressions.
         *
         * @param e les suppressions qui ont lieu dans le TextField
         */
        @Override
        public void removeUpdate( DocumentEvent e )
        {
            checkForTransfer( e.getDocument() );
        }

        /**
         * Fonction qui met à jours le component lors de changements.
         *
         * @param e le changement qui a lieu dans le TextField
         */
        @Override
        public void changedUpdate( DocumentEvent e )
        {
            checkForTransfer( e.getDocument() );
        }

        /**
         * Fonction qui vérifie si les inputs sont respectés et valide le
         * transfert.
         *
         * @param document l'input field à écouter
         */
        public void checkForTransfer( Document document )
        {
            if ( delegate != null && delegate.shouldTransferFocus( document ) )
            {
                if ( listener != null )
                {
                    listener.focusTransferShouldOccur( document );
                }
            }
        }
    }

    /**
     * Classe qui sert à vérifier que l'input introduit par l'utilisateur
     * ne contient que des nombres.
     *
     * @author Giorgio Caculli
     * @author Guillaume Lambert
     * @author Tanguy Taminiau
     * @version 1.0
     * @see InputVerifier
     */
    protected static class TextFieldInputVerifier extends InputVerifier
    {
        /**
         * Fonction qui sert à vérifier que l'input introduit par l'utilisateur
         * ne contient que des nombres.
         *
         * @param input le component à écouter
         * @return vrai si l'input contient que des nombres, faux sinon
         */
        @Override
        public boolean verify( JComponent input )
        {
            String text = ( ( JTextField ) input ).getText();
            try
            {
                BigDecimal value = new BigDecimal( text );
                return ( value.scale() <= Math.abs( 3 ) );
            } catch ( NumberFormatException nfe )
            {
                return false;
            }
        }
    }
}
