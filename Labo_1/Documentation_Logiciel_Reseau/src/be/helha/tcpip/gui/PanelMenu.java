package be.helha.tcpip.gui;

import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.Button;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

/**
 * Panel chargé en premier qui affichera le menu principale. Ce menu permettra à
 * l'utilisateur d'accéder aux différentes fonctionnalités du logiciel
 * <p>
 * @author Caculli Giorgio
 * @author Lambert Guillaume
 * @author Taminiau Tanguy
 * @version 1.0
 */
public class PanelMenu extends JPanel implements GUIDefaultValues, IDisposable, ICardHandler
{
    /** Le traducteur de chaînes de caractères */
    private final ResourceBundle rb;
    /** Bouton qui menera l'utilisateur vers la première fonction */
    private final JButton buttonFunction1 = null;
    /** Bouton qui menera l'utilisateur vers la deuxième fonction */
    private final JButton buttonFunction2 = null;
    /** Bouton qui menera l'utilisateur vers la troisième fonction */
    private final JButton buttonFunction3 = null;
    /** Bouton qui menera l'utilisateur vers la quatrième fonction */
    private final JButton buttonFunction4 = null;
    /** Bouton qui menera l'utilisateur vers la cinquième fonction */
    private final JButton buttonFunction5 = null;
    /** Bouton qui menera l'utilisateur vers le panneau 'À propos' */
    private final JButton buttonAbout = null;
    /** Bouton qui permettra à l'utilisateur d'arrêter le programme */
    private final JButton buttonQuit = null;

    /** Liste des boutons présents dans le programme */
    private final JButton[] buttons = {
            buttonFunction1,
            buttonFunction2,
            buttonFunction3,
            buttonFunction4,
            buttonFunction5,
            buttonAbout,
            buttonQuit
    };

    /**
     * Constructeur du panneau menu.
     *
     * @param rb traducteur de chaînes de caractères
     */
    public PanelMenu( ResourceBundle rb )
    {
        setLayout( new GridLayout( buttons.length, 1, 20, 20 ) );
        setBackground( backgroundColor );
        this.rb = rb;
        initButtons();
    }

    /**
     * Fonction qui sert à instancier tous les boutons correctement.
     */
    private void initButtons()
    {
        String[] buttonStrings =
                {
                        rb.getString( "button_function_1" ),
                        rb.getString( "button_function_2" ),
                        rb.getString( "button_function_3" ),
                        rb.getString( "button_function_4" ),
                        rb.getString( "button_function_5" ),
                        rb.getString( "menu_item_about" ),
                        rb.getString( "menu_item_quit" )
                };

        buttons[ 0 ] = new JButton( buttonStrings[ 0 ] );
        buttons[ 0 ].addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                showCard( getParent(), "panel.function.1" );
            }
        } );

        buttons[ 1 ] = new JButton( buttonStrings[ 1 ] );
        buttons[ 1 ].addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                showCard( getParent(), "panel.function.2" );
            }
        } );
        buttons[ 2 ] = new JButton( buttonStrings[ 2 ] );
        buttons[ 2 ].addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                showCard( getParent(), "panel.function.3" );
            }
        } );
        buttons[ 3 ] = new JButton( buttonStrings[ 3 ] );
        buttons[ 3 ].addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                showCard( getParent(), "panel.function.4" );
            }
        } );
        buttons[ 4 ] = new JButton( buttonStrings[ 4 ] );
        buttons[ 4 ].addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                showCard( getParent(), "panel.function.5" );
            }
        } );
        buttons[ 5 ] = new JButton( buttonStrings[ 5 ] );
        buttons[ 5 ].addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                showCard( getParent(), "panel.about" );
            }
        } );
        buttons[ 6 ] = new JButton( buttonStrings[ 6 ] );
        buttons[ 6 ].addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                disposeWindowConfirm( getRootPane(), rb );
            }
        } );

        for ( JButton button : buttons )
        {
            button.setFont( font );
            add( button );
        }
    }
}
