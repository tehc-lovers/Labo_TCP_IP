package be.helha.tcpip.gui;

import be.helha.tcpip.model.AdresseIP;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

/**
 * Panneau pour la fonction 5
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 * @see PanelFunctionBase
 */
public class PanelFunction5 extends PanelFunctionBase
{
    /** Affichage textuel lors d'une action */
    private final JTextArea outputTextArea = new JTextArea();

    /**
     * Constructeur pour la fonction 5.
     *
     * @param rb le traducteur de chaînes de caractères
     */
    public PanelFunction5( ResourceBundle rb )
    {
        super( rb );
        JPanel centerPanel = new JPanel( new GridLayout( 2, 1 ) );
        centerPanel.add( panelInput( rb ) );
        centerPanel.add( panelOutput( rb ) );
        centerPanel.setBackground( backgroundColor );

        add( centerPanel, BorderLayout.CENTER );
    }

    /**
     * Constructeur pour le panneau dont l'utilisateur pourra introduit son
     * input.
     *
     * @param rb le traducteur de chaînes de caractères
     * @return Le panneau contenant les inputs
     */
    private JPanel panelInput( ResourceBundle rb )
    {
        JPanel tmpPanelIPInput = new JPanel( new GridBagLayout() );
        tmpPanelIPInput.setBackground( backgroundColor );
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets( INSET_TOP, INSET_LEFT, INSET_BOTTOM, INSET_RIGHT );
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 0.5;

        JTextField[] ip1Bits = new JTextField[ 5 ];
        JTextField[] ip2Bits = new JTextField[ 5 ];
        int pos = 0;
        for ( int i = 0; i < 7; i++ )
        {
            if ( i % 2 == 0 )
            {
                ip1Bits[ pos ] = new JTextField( MAX_CHARACTERS_IPV4_BITS );
                ip1Bits[ pos ].setFont( font );
                ip1Bits[ pos ].setHorizontalAlignment( JTextField.CENTER );
                AbstractDocument doc = ( AbstractDocument ) ip1Bits[ pos ].getDocument();
                doc.setDocumentFilter( filterIPV4 );
                doc.addDocumentListener( documentListenerIPV4 );
                c.gridx = i;
                c.gridy = 1;
                tmpPanelIPInput.add( ip1Bits[ pos ], c );
            } else
            {
                pos += 1;
                JLabel dotLabel = new JLabel( "." );
                dotLabel.setFont( font );
                dotLabel.setHorizontalAlignment( JLabel.CENTER );
                c.gridx = i;
                c.gridy = 1;
                tmpPanelIPInput.add( dotLabel, c );
            }
        }
        pos = 0;
        for ( int j = 0; j < 7; j++ )
        {
            if ( j % 2 == 0 )
            {
                ip2Bits[ pos ] = new JTextField( MAX_CHARACTERS_IPV4_BITS );
                ip2Bits[ pos ].setFont( font );
                ip2Bits[ pos ].setHorizontalAlignment( JTextField.CENTER );
                AbstractDocument doc = ( AbstractDocument ) ip2Bits[ pos ].getDocument();
                doc.setDocumentFilter( filterIPV4 );
                doc.addDocumentListener( documentListenerIPV4 );
                c.gridx = j;
                c.gridy = 3;
                tmpPanelIPInput.add( ip2Bits[ pos ], c );
            } else
            {
                pos += 1;
                JLabel dotLabel = new JLabel( "." );
                dotLabel.setFont( font );
                dotLabel.setHorizontalAlignment( JLabel.CENTER );
                c.gridx = j;
                c.gridy = 3;
                tmpPanelIPInput.add( dotLabel, c );
            }
        }

        JLabel labelSlashIP1 = new JLabel( "/" );
        labelSlashIP1.setFont( font );
        labelSlashIP1.setHorizontalAlignment( JLabel.CENTER );
        c.gridx = 8;
        c.gridy = 1;
        tmpPanelIPInput.add( labelSlashIP1, c );

        JTextField maskBitIP1 = new JTextField( MAX_CHARACTERS_NETMASK );
        maskBitIP1.setFont( font );
        maskBitIP1.setHorizontalAlignment( JTextField.CENTER );
        AbstractDocument doc1 = ( AbstractDocument ) maskBitIP1.getDocument();
        doc1.setDocumentFilter( filterNetMask );
        doc1.addDocumentListener( documentListenerNetMask );
        c.gridx = 9;
        c.gridy = 1;
        tmpPanelIPInput.add( maskBitIP1, c );

        JLabel labelSlashIP2 = new JLabel( "/" );
        labelSlashIP2.setFont( font );
        labelSlashIP2.setHorizontalAlignment( JLabel.CENTER );
        c.gridx = 8;
        c.gridy = 3;
        tmpPanelIPInput.add( labelSlashIP2, c );

        JTextField maskBitIP2 = new JTextField( MAX_CHARACTERS_NETMASK );
        maskBitIP2.setFont( font );
        maskBitIP2.setHorizontalAlignment( JTextField.CENTER );
        AbstractDocument doc2 = ( AbstractDocument ) maskBitIP2.getDocument();
        doc2.setDocumentFilter( filterNetMask );
        doc2.addDocumentListener( documentListenerNetMask );
        c.gridx = 9;
        c.gridy = 3;
        tmpPanelIPInput.add( maskBitIP2, c );

        JLabel labelIP1 = new JLabel(
                String.format( "%s %d / %s",
                        rb.getString( "string_ip_address" ), 1, rb.getString( "string_netmask" ) ) );
        labelIP1.setFont( font );
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = MAX_LENGTH_IPV4_STRING_WITH_NETMASK;
        tmpPanelIPInput.add( labelIP1, c );

        JLabel labelIP2 = new JLabel(
                String.format( "%s %d / %s",
                        rb.getString( "string_ip_address" ), 2, rb.getString( "string_netmask" ) ) );
        labelIP2.setFont( font );
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = MAX_LENGTH_IPV4_STRING_WITH_NETMASK;
        tmpPanelIPInput.add( labelIP2, c );

        JButton buttonSubmit = new JButton( rb.getString( "button_submit" ) );
        buttonSubmit.addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                String bit1IP1 = ip1Bits[ 0 ].getText();
                String bit2IP1 = ip1Bits[ 1 ].getText();
                String bit3IP1 = ip1Bits[ 2 ].getText();
                String bit4IP1 = ip1Bits[ 3 ].getText();
                AdresseIP tmpAdresseIP1 = new AdresseIP( bit1IP1, bit2IP1, bit3IP1, bit4IP1 );
                String bit1IP2 = ip2Bits[ 0 ].getText();
                String bit2IP2 = ip2Bits[ 1 ].getText();
                String bit3IP2 = ip2Bits[ 2 ].getText();
                String bit4IP2 = ip2Bits[ 3 ].getText();
                AdresseIP tmpAdresseIP2 = new AdresseIP( bit1IP2, bit2IP2, bit3IP2, bit4IP2 );
                String outputString = "";
                int subnet1 = getValidator().validateNumberInput( maskBitIP1.getText() );
                int subnet2 = getValidator().validateNumberInput( maskBitIP2.getText() );
                outputString = getValidator().validateFonction5( tmpAdresseIP1, tmpAdresseIP2, subnet1, subnet2 );
                outputTextArea.setText( outputString );
            }
        } );
        buttonSubmit.setFont( font );
        c.gridx = 0;
        c.gridy = 4;
        c.gridwidth = MAX_LENGTH_IPV4_STRING_WITH_NETMASK + 1;
        tmpPanelIPInput.add( buttonSubmit, c );

        return tmpPanelIPInput;
    }

    /**
     * Fonction qui sert à créer le panneau contentant la réponse suite à
     * l'input introduit par l'utilisateur.
     *
     * @param rb traducteur de chaînes de caractères
     * @return un panneau contenant la TextArea
     */
    private JPanel panelOutput( ResourceBundle rb )
    {
        JPanel tmpPanelOutput = new JPanel( new GridBagLayout() );
        tmpPanelOutput.setBackground( backgroundColor );
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets( INSET_TOP, INSET_LEFT, INSET_BOTTOM, INSET_RIGHT );
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 0.5;

        outputTextArea.setWrapStyleWord( true );
        outputTextArea.setLineWrap( true );
        outputTextArea.setEditable( false );
        outputTextArea.setFocusable( false );
        outputTextArea.setOpaque( false );
        outputTextArea.setFont( font );
        outputTextArea.setBackground( backgroundColor );
        tmpPanelOutput.add( outputTextArea, c );

        return tmpPanelOutput;
    }
}
