package be.helha.tcpip.gui;

import java.awt.BorderLayout;
import java.util.ResourceBundle;

/**
 * Panneau visible lorsqu'on clique sur le bouton 'À propos'.
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 * @see PanelAboutBase
 * @see PanelFunctionBase
 */
public class PanelAbout extends PanelFunctionBase
{
    /**
     * Constructeur pour le panneau 'À propos'.
     *
     * @param rb traducteur des chaînes de caractères
     */
    public PanelAbout( ResourceBundle rb )
    {
        super( rb );

        add( new PanelAboutBase( rb ), BorderLayout.CENTER );
    }
}
