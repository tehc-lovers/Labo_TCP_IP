package be.helha.tcpip.gui;

import java.awt.CardLayout;
import java.awt.Container;

/**
 * Inteface qui globalise l'algorithme pour changer la carte d'un
 * CardLayout
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 * @see CardLayout
 */
public interface ICardHandler
{
    /**
     * Fonction qui sert à changer la carte d'un CardLayout
     *
     * @param container l'entité qui fait appel à la fonction
     * @param id l'id de la carte à charger
     */
    default void showCard( Container container, String id )
    {
        if ( container != null && container.getLayout() instanceof CardLayout )
        {
            CardLayout cl = ( CardLayout ) container.getLayout();
            cl.show( container, id );
        }
    }
}
