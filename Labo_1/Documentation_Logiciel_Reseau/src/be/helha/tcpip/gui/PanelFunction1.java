package be.helha.tcpip.gui;

import be.helha.tcpip.model.AdresseIP;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

/**
 * Panneau pour la fonction 1
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 * @see PanelFunctionBase
 */
public class PanelFunction1 extends PanelFunctionBase
{
    /** Affichage textuel lors d'une action */
    private final JTextArea outputTextArea = new JTextArea();

    /**
     * Constructeur pour la fonction 1.
     *
     * @param rb le traducteur de chaînes de caractères
     */
    public PanelFunction1( ResourceBundle rb )
    {
        super( rb );

        JPanel centerPanel = new JPanel( new GridLayout( 2, 0 ) );
        centerPanel.add( panelInput( rb ) );
        centerPanel.add( panelOutput( rb ) );
        centerPanel.setBackground( backgroundColor );

        add( centerPanel, BorderLayout.CENTER );
    }

    /**
     * Constructeur pour le panneau dont l'utilisateur pourra introduit son
     * input.
     *
     * @param rb le traducteur de chaînes de caractères
     * @return Le panneau contenant les inputs
     */
    private JPanel panelInput( ResourceBundle rb )
    {
        JPanel tmpPanelInput = new JPanel( new GridBagLayout() );
        tmpPanelInput.setBackground( backgroundColor );
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets( INSET_TOP, INSET_LEFT, INSET_BOTTOM, INSET_RIGHT );
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 0.5;

        JTextField[] ipBits = new JTextField[ 4 ];
        int pos = 0;
        for ( int i = 0; i < MAX_LENGTH_IPV4_STRING; i++ )
        {
            if ( i % 2 == 0 )
            {
                ipBits[ pos ] = new JTextField( MAX_CHARACTERS_IPV4_BITS );
                ipBits[ pos ].setFont( font );
                ipBits[ pos ].setHorizontalAlignment( JTextField.CENTER );
                ipBits[ pos ].setInputVerifier( new TextFieldInputVerifier() );
                AbstractDocument doc = ( AbstractDocument ) ipBits[ pos ].getDocument();
                doc.setDocumentFilter( filterIPV4 );
                doc.addDocumentListener( documentListenerIPV4 );
                c.gridx = i;
                c.gridy = 1;
                tmpPanelInput.add( ipBits[ pos ], c );
            } else
            {
                pos += 1;
                JLabel dotLabel = new JLabel( "." );
                dotLabel.setFont( font );
                dotLabel.setHorizontalAlignment( JLabel.CENTER );
                c.gridx = i;
                c.gridy = 1;
                tmpPanelInput.add( dotLabel, c );
            }
        }

        JCheckBox checkBoxClassFul = new JCheckBox( "Classful" );
        checkBoxClassFul.setBackground( backgroundColor );
        checkBoxClassFul.setFont( font );
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 5;
        tmpPanelInput.add( checkBoxClassFul, c );

        JLabel labelIP = new JLabel( rb.getString( "string_ip_address" ) );
        labelIP.setFont( font );
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = MAX_LENGTH_IPV4_STRING;
        tmpPanelInput.add( labelIP, c );

        JButton buttonSubmit = new JButton( rb.getString( "button_submit" ) );
        buttonSubmit.addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed( ActionEvent e )
            {
                String bit1 = ipBits[ 0 ].getText();
                String bit2 = ipBits[ 1 ].getText();
                String bit3 = ipBits[ 2 ].getText();
                String bit4 = ipBits[ 3 ].getText();
                AdresseIP tmpAdresseIP = new AdresseIP( bit1, bit2, bit3, bit4 );
                String outputString = "";
                outputString = getValidator().validateFonction1( tmpAdresseIP, checkBoxClassFul.isSelected() );
                outputTextArea.setText( outputString );
            }
        } );
        buttonSubmit.setFont( font );
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = MAX_LENGTH_IPV4_STRING;
        tmpPanelInput.add( buttonSubmit, c );

        return tmpPanelInput;
    }

    /**
     * Fonction qui sert à créer le panneau contentant la réponse suite à
     * l'input introduit par l'utilisateur.
     *
     * @param rb traducteur de chaînes de caractères
     * @return un panneau contenant la TextArea
     */
    private JPanel panelOutput( ResourceBundle rb )
    {
        JPanel tmpPanelOutput = new JPanel( new GridBagLayout() );
        tmpPanelOutput.setBackground( backgroundColor );
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets( INSET_TOP, INSET_LEFT, INSET_BOTTOM, INSET_RIGHT );
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 0.5;

        outputTextArea.setWrapStyleWord( true );
        outputTextArea.setLineWrap( true );
        outputTextArea.setEditable( false );
        outputTextArea.setFocusable( false );
        outputTextArea.setOpaque( false );
        outputTextArea.setFont( font );
        outputTextArea.setBackground( backgroundColor );
        tmpPanelOutput.add( outputTextArea, c );

        return tmpPanelOutput;
    }
}
