package be.helha.tcpip.model;

import java.util.ResourceBundle;

/**
 * Classe qui sert à valider tout type de l'input et renvoyer une réponse
 * dépendamment de l'input introduit par l'utilisateur
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 */
public class Validator
{
    private final Affichage affichage;
    private ResourceBundle rb = null;

    /**
     * Constructeur de la classe Validator
     *
     * @param rb le traducteur des différentes chaînes de caractères
     */
    public Validator( ResourceBundle rb )
    {
        this.rb = rb;
        affichage = new Affichage( rb );
    }

    /**
     * Fonction qui sert à valider si l'input de l'utilisateur est
     * effectivement un nombre qui ne contient pas de symboles.
     *
     * @param numberInput l'input introduit par l'utilisateur
     * @return le nombre si le input est bon, -1 si c'est pas le cas
     */
    public int validateNumberInput( String numberInput )
    {
        try
        {
            return Integer.parseInt( numberInput );
        } catch ( NumberFormatException nfe )
        {
            return -1;
        }
    }

    /**
     * Fonction qui sert à vérifier la validité des octets d'une adresse IP
     *
     * @param adresseIP l'adresse IP à vérifier
     * @return vrai si l'adresse IP est conforme, sinon faux
     */
    private boolean invalidIPV4( AdresseIP adresseIP )
    {
        /* Le point en REGEX s'écrit avec \\. */
        String[] adresseIPBits = adresseIP.getOctetsDecimal().split( "\\." );
        if ( adresseIPBits.length < 4 )
        {
            return true;
        }
        try
        {
            for ( String s : adresseIPBits )
            {
                if ( validateNumberInput( s ) < 0 || validateNumberInput( s ) > 255 )
                {
                    return true;
                }
            }
            return false;
        } catch ( NumberFormatException nfe )
        {
            return true;
        }
    }

    /**
     * Fonction qui sert à vérifier la validité des octets d'une adresse masque.
     *
     * @param adresseMasque l'adresse masque à vérifier
     * @return vrai si l'adresse masque est conforme, sinon faux
     */
    private boolean invalidMask( AdresseMasque adresseMasque )
    {
        String[] adresseMasqueBits = adresseMasque.getOctetsDecimal().split( "\\." );
        if ( adresseMasqueBits.length < 4 )
        {
            return true;
        }
        int[] octetsMasque= adresseMasque.getOctetsAdresse();
        for(int i = 0; i<octetsMasque.length-1; i++) {
            if(octetsMasque[i]!=255 && octetsMasque[i+1]!=0) {
                return true;
            }
        }
        try
        {
            for ( String s : adresseMasqueBits )
            {
                if ( validateNumberInput( s ) < 0 || validateNumberInput( s ) > 255 )
                {
                    return true;
                }
            }
            return false;
        } catch ( NumberFormatException nfe )
        {
            return true;
        }
    }

    /**
     * Fonction qui sert à récupérer la classe d'une adresse IP.
     *
     * @param adresseIP l'adresse IP dont nous souhaitons récupérer la classe
     * @param classFul si vrai, sortie textuelle plus détaillée
     * @return chaîne de caractères avec la classe propre à l'adresse IP
     */
    public String validateFonction1( AdresseIP adresseIP, boolean classFul )
    {
        if ( invalidIPV4( adresseIP ) )
        {
            return rb.getString( "string_invalid_components" );
        }
        return affichage.getTypeStr( adresseIP, classFul );
    }

    /**
     * Fonction qui sert à récupérer les adresses de réseau et sous-réseau.
     *
     * @param adresseIP l'adresse à partir de laquelle on récupérera les adresse réseaux.
     * @param adresseMasque masque qui déterminera l'adresse de réseau et sous-réseau
     * @return chaîne de caractères avec les réseaux et sous-réseaux
     */
    public String validateFonction2( AdresseIP adresseIP, AdresseMasque adresseMasque )
    {
        boolean sousReseau = false;
        if ( invalidIPV4( adresseIP ) || invalidMask( adresseMasque ) )
        {
            return rb.getString( "string_invalid_components" );
        }
        for ( String s : adresseMasque.getOctetsDecimal().split( "\\." ) )
        {
            int bit = Integer.parseInt( s );
            if ( bit != 255 && bit != 0 )
            {
                sousReseau = true;
            }
        }
        return affichage.getAdresseReseauEtBroadcast( adresseIP, adresseMasque, sousReseau );
    }

    /**
     * Fonction qui sert à vérifier si deux adresses font partie du même réseau.
     *
     * @param adresseIP1 adresse IP du réseau
     * @param adresseIP2 adresse à vérifier
     * @param subnet subnet propre à l'adresse de réseau
     * @return sortie textuelle qui confirme si l'adresse IP fait effectivement partie du réseau
     */
    public String validateFonction3( AdresseIP adresseIP1, AdresseIP adresseIP2, int subnet )
    {
        if ( invalidIPV4( adresseIP1 ) || invalidIPV4( adresseIP2 ) || subnet < 0 || subnet > 32 )
        {
            return rb.getString( "string_invalid_components" );
        }
        return affichage.faitPartieDuReseauStr( adresseIP1, adresseIP2, subnet );
    }

    public String validateFonction4( AdresseIP adresseIP1, AdresseIP adresseIP2, int subnet )
    {
        if ( invalidIPV4( adresseIP1 ) || invalidIPV4( adresseIP2 ) || subnet < 0 || subnet > 32 )
        {
            return rb.getString( "string_invalid_components" );
        }
        return affichage.faitPartieDuReseauMachineStr( adresseIP1, adresseIP2, subnet );
    }

    /**
     * Fonction qui sert à déterminer si deux adresses IP avec subnets différents feraient
     * partie ou non d'un même réseau.
     *
     * @param adresseIP1 l'adresse IP dans un réseau
     * @param adresseIP2 l'adresse IP dans un autre réseau
     * @param subnet1 la subnet de la première adresse IP
     * @param subnet2 la subnet de la deuxième adresse IP
     * @return retour textuel qui confirme si les deux font partie d'un même réseau
     */
    public String validateFonction5( AdresseIP adresseIP1, AdresseIP adresseIP2, int subnet1, int subnet2 )
    {
        if ( invalidIPV4( adresseIP1 ) || invalidIPV4( adresseIP2 ) || subnet1 < 0 || subnet1 > 32 || subnet2 < 0 || subnet2 > 32 )
        {
            return rb.getString( "string_invalid_components" );
        }
        return affichage.faitPartieDuReseauStr( adresseIP1, adresseIP2, subnet1, subnet2 );
    }
}
