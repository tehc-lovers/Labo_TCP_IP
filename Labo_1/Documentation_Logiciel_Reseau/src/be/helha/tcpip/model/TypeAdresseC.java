package be.helha.tcpip.model;

/**
 * Instanciation d'une adresse de classe C
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 * @see TypeAdresse
 */
public class TypeAdresseC extends TypeAdresse
{
    /**
     * Constructeur pour le type d'adresse C avec le masque par défaut.
     */
    public TypeAdresseC()
    {
        super( new AdresseMasque( "255", "255", "255", "0" ) );
    }

    /**
     * Fonction qui renvoie sa classe sous forme textuelle.
     *
     * @return "C"
     */
    @Override
    public String getTypeStr()
    {
        return "C";
    }
}
