package be.helha.tcpip.model;

/**
 * Classe qui va servir à convertir des octets soit en binaire soit en décimal.
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 */
public class Convertisseur implements IConversion
{
    /**
     * Fonction qui va convertir des octets décimaux en octets binaires.
     *
     * @param octetsDecimal tableau d'octets décimaux
     * @return un tableau d'octets en format binaire
     */
    @Override
    public String[] conversionBinaire( String[] octetsDecimal )
    {
        String[] retourBinaire = new String[ 4 ];
        int i = 0;
        for ( String octet : octetsDecimal )
        {
            int stringOctet;
            try
            {
                stringOctet = Integer.parseInt( octet );
                retourBinaire[ i ] = String.format( "%8s", Integer.toBinaryString( stringOctet ) ).replace( " ", "0" );
            } catch ( NumberFormatException nfe )
            {
                return new String[]{};
            }
            i++;
        }

        return retourBinaire;
    }

    /**
     * Fonction qui va convertir des octets binaires en octets décimaux.
     *
     * @param octetsBinaire tableau d'octets binaires
     * @return un tableau d'octets en format décimal
     */
    @Override
    public String[] conversionDecimal( String[] octetsBinaire )
    {
        String[] retourDecimal = new String[ 4 ];
        int i = 0;
        for ( String octet : octetsBinaire )
        {
            int stringOctet;
            try
            {
                stringOctet = Integer.parseInt( octet, 2 );
            } catch ( NumberFormatException nfe )
            {
                return new String[]{};
            }
            retourDecimal[ i ] = Integer.toString( stringOctet );
            i++;
        }

        return retourDecimal;
    }

}
