package be.helha.tcpip.model;

import java.util.ResourceBundle;

/**
 * Classe parent de tout type de classe d'adresse
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 */
public abstract class TypeAdresse implements ITypeAdresse
{
    private final AdresseMasque masqueReseauDefault;
    private ResourceBundle rb = ResourceBundle.getBundle( "be.helha.tcpip.res.strings" );

    /**
     * Instanciation de base du masque de réseau par défaut
     *
     * @param masqueReseauDefault le masque de réseau par défaut de chaque classe
     */
    public TypeAdresse( AdresseMasque masqueReseauDefault )
    {
        this.masqueReseauDefault = masqueReseauDefault;
    }

    /**
     * Cette fonction sert à renvoyer une nouvelle instance du type d'adresse.
     *
     * @param adresseIP l'adresse IP dont nous souhaitons récupérer sa classe
     * @return la nouvelle instance propre au type de classe
     */
    public TypeAdresse getType( AdresseIP adresseIP )
    {
        int premierBit = Integer.parseInt( adresseIP.getOctetsDecimal().split( "\\." )[ 0 ] );
        if ( premierBit >= EMinMaxIP.A.getMin() && premierBit <= EMinMaxIP.A.getMax() )
        {
            return new TypeAdresseA();
        } else if ( premierBit >= EMinMaxIP.B.getMin() && premierBit <= EMinMaxIP.B.getMax() )
        {
            return new TypeAdresseB();
        } else if ( premierBit >= EMinMaxIP.C.getMin() && premierBit <= EMinMaxIP.C.getMax() )
        {
            return new TypeAdresseC();
        } else
        {
            return new TypeAdresseIndeterminee();
        }
    }

    /**
     * Fonction qui sert à renvoyer une chaîne de caractère propre à la
     * nouvelle instanciation de getType.
     *
     * @return le type de classe en format textuel
     */
    public abstract String getTypeStr();

    public String toString()
    {
        String output = "";
        output += String.format( "%s: %s\n",
                rb.getString( "string_type_of_address" ), getTypeStr() );
        output += String.format( "%s: %s\n",
                rb.getString( "string_number_of_networks" ), masqueReseauDefault.getNombreDeReseaux() );
        output += String.format( "%s: %d\n",
                rb.getString( "string_number_of_hosts" ), masqueReseauDefault.getNombreDeHotes() );
        return output;
    }
}
