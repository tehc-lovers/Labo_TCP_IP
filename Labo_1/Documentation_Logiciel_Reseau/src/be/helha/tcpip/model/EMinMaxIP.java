package be.helha.tcpip.model;

/**
 * Enumeration qui sert à établir les limites entre les différents types de
 * classes d'adresse.
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 */
public enum EMinMaxIP
{
    /** Adresse de classe A */
    A( 0, 126 ),
    /** Adresse de classe B */
    B( 128, 191 ),
    /** Adresse de classe C */
    C( 192, 223 );

    /** Valeur minimale du bit */
    private final int min;
    /** Valeur maximale du bit */
    private final int max;

    /**
     * Constructeur utilisé pour établir les différentes valeurs.
     *
     * @param min la valeur minimale du premier octet
     * @param max la valeur maximale du premier octet
     */
    EMinMaxIP( int min, int max )
    {
        this.min = min;
        this.max = max;
    }

    /**
     * Getter sur la valeur minimale
     *
     * @return la valeur minimale du premier octet d'un type de classe
     */
    public final int getMin()
    {
        return min;
    }

    /**
     * Getter sur la valeur maximale
     *
     * @return la valeur maximale du premier octet d'un type de classe
     */
    public final int getMax()
    {
        return max;
    }
}
