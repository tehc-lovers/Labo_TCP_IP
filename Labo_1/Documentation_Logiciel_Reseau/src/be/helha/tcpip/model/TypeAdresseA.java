package be.helha.tcpip.model;

/**
 * Instanciation d'une adresse de classe A
 *
 * @author Giorgio Caculli
 * @author Guillaume Lambert
 * @author Tanguy Taminiau
 * @version 1.0
 * @see TypeAdresse
 */
public class TypeAdresseA extends TypeAdresse
{
    /**
     * Constructeur pour le type d'adresse A avec le masque par défaut.
     */
    public TypeAdresseA()
    {
        super( new AdresseMasque( "255", "0", "0", "0" ) );
    }

    /**
     * Fonction qui renvoie sa classe sous forme textuelle.
     *
     * @return "A"
     */
    @Override
    public String getTypeStr()
    {
        return "A";
    }
}
